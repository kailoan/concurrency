package sorting;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveAction;

/**
 * Extends RecursiveAction.
 * Notice that the compute method does not return anything.
 */
public class MergeTask5 extends RecursiveAction {
    private List<Integer> array;
    private int left;
    private int right;

    public MergeTask5(List<Integer> array, int left, int right) {
        this.array = array;
        this.left = left;
        this.right = right;
    }

    /**
     * Inherited from RecursiveAction.
     * Compare it with the run method of a Thread.
     */
    @Override
    protected void compute() {
        if (left < right) {
            int mid = (left + right) / 2;
            RecursiveAction leftSort = new MergeTask5(array, left, mid);
            RecursiveAction rightSort = new MergeTask5(array, mid + 1, right);
            invokeAll(leftSort, rightSort);
            merge(left, mid, right);
        }
    }

    /**
     * Merge two parts of an array in sorted manner.
     *
     * @param left  Left side of left array.
     * @param mid   Middle of separation.
     * @param right Right side of right array.
     */
    private void merge(int left, int mid, int right) {
        List<Integer> temp = fillInSomeRandomNumbers(right - left + 1);

        int x = left;
        int y = mid + 1;
        int z = 0;

//There some kind of sort at the leaf
//You can use your sorting.BubbleSort class
//***************************************************************
        while (x <= mid && y <= right) {
            if (array.get(x) <= array.get(y)) {
                //temp[z] = array[x];
                temp.set(z,array.get(x));
                z++;
                x++;
            } else {
                //temp[z] = array[y];
                temp.set(z,array.get(y));
                z++;
                y++;
            }
        }
//***************************************************************
        //BubbleSort.sort(temp);

        while (y <= right) {
            //temp[z++] = array[y++];
            temp.set(z++,array.get(y++));
        }
        while (x <= mid) {
            //temp[z++] = array[x++];
            temp.set(z++,array.get(x++));
        }

        for (z = 0; z < temp.size(); z++) {
            //array[left + z] = temp[z];
            array.set(left+z,temp.get(z));
        }
    }


    private List<Integer> fillInSomeRandomNumbers(int size){
        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            numbers.add(-1);
        }

        return numbers;

    }

}
