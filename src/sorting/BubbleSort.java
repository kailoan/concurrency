package sorting;

import java.util.ArrayList;
import java.util.List;

public class BubbleSort {

    public static void sort(List<Integer> arr){
        int n = arr.size();
        int temp = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (arr.get(j - 1) > arr.get(j)) {
                    //swap elements
                    temp = arr.get(j - 1);
                    arr.set(j - 1, arr.get(j));
                    arr.set(j, temp);
                }
            }
        }
    }

}
