package sorting;

import java.util.ArrayList;
import java.util.List;

public class OperationHelper {

    public static List<Integer> mergeArrays(List<Integer> firstPart, List<Integer> secondPart, int n1,
                                            int n2, List<Integer> finalArray)
    {
        int i = 0, j = 0, k = 0;

        // Traverse both array
        while (i<n1 && j <n2)
        {
            // Check if current element of first
            // array is smaller than current element
            // of second array. If yes, store first
            // array element and increment first array
            // index. Otherwise do same with second array
            if (firstPart.get(i) < secondPart.get(j))
                finalArray.set(k++,firstPart.get(i++));
            else
                finalArray.set(k++,secondPart.get(j++));
        }

        // Store remaining elements of first array
        while (i < n1)
            finalArray.set(k++,firstPart.get(i++));

        // Store remaining elements of second array
        while (j < n2)
            finalArray.set(k++,secondPart.get(j++));

        return finalArray;
    }

    public static boolean isSortedArray(List<Integer> array){
        boolean result = true;
        int i;

        for (i = 0; i < array.size()-1; i++) {
            if(array.get(i) > array.get(i+1)) {
                result = false;
                break;
            }
        }

        //if(!result) System.out.println("Error at number # " + i + ", the values were: " + array.get(i-1)
        // + ", " + array.get(i) + ".");

        return result;

    }

    public static void setupArray(List<Integer> numbers, int amountOfNumbers){
        for (int i = 0; i < amountOfNumbers; i++) {
            numbers.add((int) (Math.random() * 10000));
        }
    }


    public static void outputResults(double[][] results, int NUMBER_CYCLES){
        List<Double> finalResult = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            double average = 0;
            for (int j = 0; j < NUMBER_CYCLES; j++) {
                average += results[i][j];
            }

            average /= NUMBER_CYCLES;
            finalResult.add(average);
        }

        System.out.println("\n\n\n============================\n\n");
        System.out.println("Sorting took " + finalResult.get(0) + " millis for 25.000 numbers");
        System.out.println("Sorting took " + finalResult.get(1) + " millis for 50.000 numbers");
        System.out.println("Sorting took " + finalResult.get(2) + " millis for 100.000 numbers");
        System.out.println("Sorting took " + finalResult.get(3) + " millis for 200.000 numbers");
        System.out.println("Sorting took " + finalResult.get(4) + " millis for 400.000 numbers");
    }

}
