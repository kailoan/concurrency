package sorting;

import tasks.Task4;

import java.util.ArrayList;
import java.util.List;

public class SortingThreadTask4 implements Runnable{

    private List<Integer> numbers;

    public SortingThreadTask4(List<Integer> numbers){
        this.numbers = numbers;
    }

    @Override
    public void run() {
        sort(numbers);
    }

    private List<Integer> sort(List<Integer> array){
        if(array.size() > Task4.THRESHOLD){//if array size is higher than THRESHOLD
            //half of array
            List<Integer> firstHalf = new ArrayList<>(array.subList(0,array.size()/2));
            List<Integer> secondHalf = new ArrayList<>(array.subList(array.size()/2, array.size()));

            //runnable with half array
            SortingThreadTask4 runnable1 = new SortingThreadTask4(firstHalf);
            SortingThreadTask4 runnable2 = new SortingThreadTask4(secondHalf);

            Thread firstThread = new Thread(runnable1);
            Thread secondThread = new Thread(runnable2);

            //start 2 threads
            firstThread.start();
            secondThread.start();

            try {//wait for threads to finish working
                firstThread.join();
                secondThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //merge 2 halves
            BubbleSort.mergeArrays(firstHalf,secondHalf,firstHalf.size(),secondHalf.size(),array);
        } else {//if size of array is less than THRESHOLD -> just sort
            BubbleSort.sort(array);
        }

        return array;
    }

}
