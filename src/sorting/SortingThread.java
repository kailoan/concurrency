package sorting;

import java.util.List;

public class SortingThread implements Runnable {

    List<Integer> numbers;

    public SortingThread(List<Integer> numbers){
        this.numbers = numbers;
    }

    @Override
    public void run() {
        bubbleSort();
    }

    private void bubbleSort() {
        List<Integer> arr = numbers;
        int n = arr.size();
        int temp = 0;
        for(int i=0; i < n; i++){
            for(int j=1; j < (n-i); j++){
                if(arr.get(j-1) > arr.get(j)){
                    //swap elements
                    temp = arr.get(j-1);
                    arr.set(j-1, arr.get(j));
                    arr.set(j, temp);
                }
            }
        }
    }

    public List<Integer> getNumbers() {
        return numbers;
    }
}
