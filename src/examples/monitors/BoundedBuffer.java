package examples.monitors;

public class BoundedBuffer <E>  extends BaseBoundedBuffer<E> {

    protected BoundedBuffer(int capacity) {
        super(capacity);
    }


    /*

    use the protected method of BaseBoundedBuffer to implement the following public methods

        put  // puts an element into the buffer. If the buffer is full it neets to wait
                for the buffer to become not full
        take // take an element from the buffer. If the buffer is emtpty it needs to wait
                for the buffer to become not empty

     */
    synchronized void put(E item) {
        while (isFull()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        doPut(item);
        notify();
    }

    synchronized E take() {
        E item;
        while (isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        item = doTake();
        notify();
        return item;
    }

}
