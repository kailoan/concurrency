package examples.monitors;

import java.util.Random;

class Producer extends Thread{



    private final BoundedBuffer<Data> buffer;


    public Producer(String name,BoundedBuffer<Data> buffer){
        super(name);
        this.buffer = buffer;
    }


    public void run(){
        Random r = new Random();
        while (true) {

            /* produce some data */
            Data data = new Data(r.nextInt(1000));
            /* and out it in the buffer */
            buffer.put(data);
        }
    }
}
