package examples.monitors;

public class ProducerConsumerEnv {


    public static void main(String[] args) {
        new ProducerConsumerEnv().startEnv();
    }



    public void startEnv(){

        BoundedBuffer<Data> buffer;
        int NROFELEMENTS = 5;
        int NROFCONSUMERS =3;
        int NROFPRODUCERS =2;

        buffer = new BoundedBuffer(NROFELEMENTS);

        for( int i=0; i<NROFCONSUMERS; i++){
            new Consumer("c"+i,buffer).start();
        }
        for( int i=0; i<NROFPRODUCERS; i++){
            new Producer("p"+i,buffer).start();
        }
    }

}
