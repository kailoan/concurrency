package examples.monitors;

import java.util.Random;

class Consumer extends Thread {

    private final BoundedBuffer<Data> buffer;


    public Consumer(String name,BoundedBuffer<Data> buffer){
        super(name);
        this.buffer = buffer;
    }

    public void run(){
        while (true) {
            Random r = new Random();
            Data d;

            d = buffer.take();
            /* Consume some data from the buffer */
            try {
                Thread.currentThread().sleep(1000 + r.nextInt(500));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
