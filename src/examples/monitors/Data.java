package examples.monitors;

public class Data {
    String data;

    public Data(int i) {
        data = "" + i;
    }

    public String getData() {
        return data;
    }
}
