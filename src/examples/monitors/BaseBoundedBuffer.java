package examples.monitors;

public abstract class BaseBoundedBuffer<E> {


    /** The buffer*/
    private final E[] buf;

    /** first free position in buffer */
    private int tail = 0;

    /** first element in buffer */
    private int head = 0;

    /** nr of elements in buffer */
    private int count = 0;


    protected BaseBoundedBuffer(int capacity) {
        buf = (E[]) new Object[capacity];
    }


    /*
        implement the following protected methods:
        isFull  //returns true if the element is full
        isEmpty //returns true if the element is empty
        doPut   //insert an element at the tail of the buffer and increment the tail
        doTake  //extract an element at the head of the buffer and increment the head

    */
    boolean isFull() {
        return count == buf.length;
    }

    boolean isEmpty() {
        return count == 0;
    }

    void doPut(E item) {
        buf[tail] = item;
        System.out.println("Write " + tail + " " + ((Data)item).getData());
        tail++;
        if (tail == buf.length) tail = 0;
        count++;
    }

    E doTake() {
        E item;
        item = buf[head];
        buf[head] = null;
        if (item != null) System.out.println("Read " + head + " " + ((Data)item).getData());
        head++;
        if(head == buf.length) head = 0;
        count--;
        return item;
    }
}
