package examples.semaphores;

public class Main {
    public static void main(String[] args) {
        MyBuffer buffer = new MyBuffer(5);

        Producer producer = new Producer(buffer);
        Consumer consumer = new Consumer(buffer);

        Thread cons = new Thread(consumer);

        Thread prod = new Thread(producer);

        cons.start();
        prod.start();
    }
}
