package examples.semaphores;

import java.util.concurrent.Semaphore;

public class MyBuffer {
    String theBuffer[];
    int size;
    Semaphore writeSem;
    Semaphore readSem;
    int writePointer = 0;
    int readPointer = 0;

    public MyBuffer(int s) {
        this.size = s;

        theBuffer = new String[size];
        writeSem = new Semaphore(size);
        readSem = new Semaphore(0);
    }

    public void write(String data) {
        try {
            writeSem.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        theBuffer[writePointer] = data;
        System.out.println("Writing: " + data);
        writePointer++;

        if (writePointer == size) {
            writePointer = 0;
        }

        readSem.release();
    }

    public String read() {
        return "";
    }
}
