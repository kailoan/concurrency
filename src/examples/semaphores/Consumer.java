package examples.semaphores;

import java.util.Random;

public class Consumer implements Runnable {
    MyBuffer buffer;

    public Consumer(MyBuffer b) {
        this.buffer = b;
    }

    @Override
    public void run() {
        String data;
        Random r = new Random();
        while(true) {
            data = buffer.read();

            System.out.println("reading: " + data);

            try {
                Thread.currentThread().sleep(1000 + r.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
