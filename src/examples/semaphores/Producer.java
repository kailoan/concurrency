package examples.semaphores;

import java.util.Random;

public class Producer implements Runnable {
    MyBuffer buffer;

    public Producer(MyBuffer b) {
        this.buffer = b;
    }

    @Override
    public void run() {
        String data;
        Random r = new Random();
        while(true) {
            // make data
            data = "Text " + r.nextInt(1000);

            // write data to buffer
            buffer.write(data);

            // sleep
            try {
                Thread.currentThread().sleep(200 + r.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
