package launcher;

import tasks.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        new Main().run();
    }

    private void run() {
        boolean work = true;
        Scanner scanner = new Scanner(System.in);
        while (work) {
            displayMenu();
            switch (scanner.nextInt()) {
                case 0: {
                    work = false;
                    System.out.println("\nGood bye!");
                    break;
                }
                case 1: {
                    System.out.println("Starting task 1");
                    Task1 task1 = new Task1();
                    System.out.println("Task 1 finished");
                }
                case 2: {
                    System.out.println("Starting task 2");
                    Task2 task2 = new Task2();
                    System.out.println("Task 2 finished");
                    break;
                }
                case 3: {
                    System.out.println("Starting task 3");
                    Task3 task3 = new Task3();
                    System.out.println("Task 3 finished");
                    break;
                }
                case 4: {
                    System.out.print("Enter threshold -> ");
                    int threshold = scanner.nextInt();

                    System.out.println("Starting task 4");
                    Task4 task4 = new Task4(threshold);
                    System.out.println("Task 4 finished");
                    break;
                }
                case 5: {
                    System.out.println("Starting task 5");
                    Task5 task5 = new Task5();
                    System.out.println("\nTask 5 finished");
                    break;
                }
                default: {
                    System.err.println("Enter correct number and try again\n");
                }
            }
        }
    }

    private void displayMenu() {
        System.out.print(
                "\nWelcome to threading task\n"
                        + "=======================\n"
                        + "1) Run task 1 (sorting using single thread)\n"
                        + "2) Run task 2 (sorting using single thread divided by 2 sub arrays)\n"
                        + "3) Run task 3 (sorting using 2 threads)\n"
                        + "4) Run task 4 (sorting using multiple threads dividing array by every " + Task4.THRESHOLD + " elements)\n"
                        + "5) Run task 5 (sorting using forkJoin thread pool)\n"
                        + "0) Exit\n"
                        + "=======================\n"
                        + "Enter number of task to run -> ");
    }

}
