package tasks;

import sorting.BubbleSort;
import sorting.OperationHelper;
import sorting.SortingThread;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Task3 {

    public static final int NUMBER_CYCLES = 2;

    public static void main(String[] args) {
        new Task3().run();
    }

    public Task3(){
        run();
    }

    private void run() {
        List<Integer> numbers = new ArrayList<>();

        // create the result matrix
        double results[][] = new double[5][NUMBER_CYCLES];

        int resultId = 0;

        int amountOfNumbers = 25000;

        while (amountOfNumbers <= 400000) {

            for (int j = 0; j < NUMBER_CYCLES; j++) {
                // fill the list with random values
                OperationHelper.setupArray(numbers,amountOfNumbers);

                System.out.println("Size of full array: " + numbers.size());

                // separate list into two sub lists
                List<Integer> firstPart = numbers.subList(0, numbers.size() / 2);
                List<Integer> secondPart = numbers.subList(numbers.size() / 2, numbers.size());

                // associate a thread for the bubble sorting of each list
                Thread firstThread = new Thread(new SortingThread(firstPart));
                Thread secondThread = new Thread(new SortingThread(secondPart));

                Instant begin = Instant.now();

                // start the sorting
                firstThread.start();
                secondThread.start();

                // conclude the sorting
                try {
                    firstThread.join();
                    secondThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // merge the results into the numbers array
                OperationHelper.mergeArrays(firstPart, secondPart, firstPart.size(), secondPart.size(), numbers);

                System.out.println("Size of full array after: " + numbers.size());

                Instant end = Instant.now();
                Duration duration = Duration.between(begin, end);

                results[resultId][j] = duration.toMillis();

                // empty the list
                numbers.clear();

                System.out.println("It took: " + duration);
            }
            amountOfNumbers = amountOfNumbers * 2;
            resultId++;
        }
        OperationHelper.outputResults(results,NUMBER_CYCLES);
    }
}
