package tasks;

import sorting.BubbleSort;
import sorting.OperationHelper;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Task2 {

    public static final int NUMBER_CYCLES = 2;

    public static void main(String[] args) {
        new Task2().run();
    }

    public Task2(){
        run();
    }

    private void run() {
        List<Integer> numbers = new ArrayList<>();

        double[][] results = new double[5][NUMBER_CYCLES];//records array

        int resultId = 0;

        int amountOfNumbers = 25000;

        while(amountOfNumbers <= 400000) {

            for (int j = 0; j < NUMBER_CYCLES; j++) {
                OperationHelper.setupArray(numbers,amountOfNumbers);
                //divide array into 2 halves
                List<Integer> firstPart = numbers.subList(0, numbers.size() / 2);
                List<Integer> secondPart = numbers.subList(numbers.size() / 2, numbers.size());

                System.out.println("First sub array size: " + firstPart.size());
                System.out.println("Second sub array size: " + firstPart.size());

                Instant begin = Instant.now();

                //sort 2 halves
                BubbleSort.sort(firstPart);
                BubbleSort.sort(secondPart);

                //merge 2 halves
                OperationHelper.mergeArrays(firstPart, secondPart, firstPart.size(), secondPart.size(), numbers);

                System.out.println("Size of full array after: " + numbers.size());

                Instant end = Instant.now();
                Duration duration = Duration.between(begin, end);

                results[resultId][j] = duration.toMillis();

                numbers.clear();

                System.out.println("It took: " + duration);
            }

            resultId++;
            amountOfNumbers *= 2;

        }

        OperationHelper.outputResults(results,NUMBER_CYCLES);

    }


}
