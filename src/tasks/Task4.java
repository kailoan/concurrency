package tasks;

import sorting.BubbleSort;
import sorting.OperationHelper;
import sorting.SortingThreadTask4;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Task4 {
    /*
    2 - 25-27
    5 - 5sec.
    10 -0.9 sec.
    20 - 0.3 sec.
    50 - 0.08 sec.
    75 - 0.08 sec.
    100 - 0.08 sec.
    200 - 0.06 sec.
    400 - 0.1 sec.
     */

    public static int THRESHOLD = 200; //to divide array before it`s size is less than threshold
    private int amountOfNumbers = 25000;

    private static final int NUMBER_CYCLES = 3;//how many times one amount of numbers will be sorted

    public static void main(String[] args) {
        new Task4().run();
    }

    public Task4(){
        run();
    }

    public Task4(int threshold){
        THRESHOLD = threshold;
        run();
    }

    private void run(){
        double[][] results = new double[5][NUMBER_CYCLES];

        int resultId = 0;

        while(amountOfNumbers <= 400000) {
            for (int i = 0; i < NUMBER_CYCLES; i++) { //one amount of numbers are sorted NUMBER_CYCLES times
                results[resultId][i] = proceed();//write milliseconds to records array
            }
            resultId++;
            amountOfNumbers *= 2;
        }

        OperationHelper.outputResults(results,NUMBER_CYCLES);
    }

    private double proceed(){
        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < amountOfNumbers; i++) {
            numbers.add((int) (Math.random() * 100000));
        }
        SortingThreadTask4 runnable1 = new SortingThreadTask4(numbers); //creating runnable with array to sort

        //create thread
        Thread firstThread = new Thread(runnable1);

        Instant begin = Instant.now();

        //start thread
        firstThread.start();

        try {//wait until thread finishes it`s work
            firstThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Instant end = Instant.now();
        Duration duration = Duration.between(begin, end);

        System.out.println("It took: " + duration.toMillis() + " milliseconds from " + numbers.size() + " numbers.");

        return duration.toMillis();
    }

}
