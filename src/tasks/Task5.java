package tasks;

import sorting.BubbleSort;
import sorting.MergeTask5;
import sorting.OperationHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * Example of Merge Sort using Fork/Join framework.
 *
 * @author L.Gobinath
 */
public class Task5 {
    // From Java 7 '_' can be used to separate digits.

    private static final int NUMBER_CYCLES = 2;

    public static void main(String[] args) {
        new Task5().run();
    }

    public Task5(){
        run();
    }

    private void run(){
        double[][] results = new double[5][NUMBER_CYCLES];

        int resultId = 0;

        int amountOfNumbers = 25000;

        while(amountOfNumbers <= 400000) {
            for (int i = 0; i < NUMBER_CYCLES; i++) {
                results[resultId][i] = process(amountOfNumbers);
            }
            resultId++;
            amountOfNumbers *= 2;
        }

        OperationHelper.outputResults(results,NUMBER_CYCLES);

    }

    private double process(int arraySize){
        // Create a pool of threads

        ForkJoinPool pool = new ForkJoinPool();
        List<Integer> array = createArray(arraySize);
        long startTime;
        long endTime;

        MergeTask5 mergeSort = new MergeTask5(array, 0, array.size() - 1);
        startTime = System.currentTimeMillis();

        pool.invoke(mergeSort); // Start execution and wait for result/return

        if(OperationHelper.isSortedArray(array)) System.out.println("Array is sorted");
        else System.out.println("Array is NOT sorted");

        endTime = System.currentTimeMillis();
        System.out.println("Time taken: " + (endTime - startTime) + " millis");

        return endTime - startTime;
    }

    /**
     * Create an array with random numbers.
     * @param  size Size of the array.
     * @return      An array with the given size.
     */
    private static List<Integer> createArray(final int size) {
        List<Integer> array = new ArrayList<>();
        Random rand = new Random();
        for (int i = 0; i < size; i++) {
            array.add(rand.nextInt(1000));
        }
        return array;
    }
}