package tasks;

import sorting.BubbleSort;
import sorting.OperationHelper;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Task1 {

    public static final int NUMBER_CYCLES = 1;

    public static void main(String[] args) {
        new Task1().run();
    }

    public Task1(){
        run();
    }

    private void run() {
        List<Integer> numbers = new ArrayList<>();

        // create the result matrix which saves our results for all 5 sizes and is repeated NUMBER_CYCLES times
        double[][] results = new double[5][NUMBER_CYCLES];

        int resultId = 0;

        int amountOfNumbers = 25000;

        // continue running the program until we reach the biggest size of elements
        while (amountOfNumbers <= 400000) {

            for (int i = 0; i < NUMBER_CYCLES; i++) {

                OperationHelper.setupArray(numbers,amountOfNumbers);
                // fill our list with random numbers
                OperationHelper.setupArray(numbers,amountOfNumbers);

                // start the timer to detect how much time it took to completed the sorting
                Instant begin = Instant.now();

                // bubble sort the numbers list
                BubbleSort.sort(numbers);

                Instant end = Instant.now();
                Duration duration = Duration.between(begin, end);

                System.out.println((i + 1) + ") it took: " + ((double) (duration.toMillis() / 1000)) + " sec. of " + amountOfNumbers + " numbers");

                results[resultId][i] = duration.toMillis();

                // empty the list
                numbers.clear();
            }

            resultId++;

            amountOfNumbers *= 2;

        }

        OperationHelper.outputResults(results, NUMBER_CYCLES);
    }
}
